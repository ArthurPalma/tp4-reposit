﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TP4;

namespace Test2TP4
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAire()
        {
            int longueur = 4;
            Assert.AreEqual(16, Program.aireCarre(longueur));
            int longueur2 = 4;
            Assert.AreEqual(16, Program.aireCarre(longueur2));
        }
        [TestMethod]
        public void TestRectangle()
        {
            int longueur = 4, largeur = 2;
            Assert.AreEqual(8, Program.aireRectangle(longueur, largeur));
            int longueur2 = 4, largeur2 = 2;
            Assert.AreEqual(8, Program.aireRectangle(longueur2, largeur2));
        }
        [TestMethod]
        public void TestTriangle()
        {
            int basee = 4, hauteur = 6;
            Assert.AreEqual(12, Program.aireTriangle(basee, hauteur));
            int base1 = 4, hauteur2 = 6;
            Assert.AreEqual(12, Program.aireTriangle(base1, hauteur2));
        }

    }
}
